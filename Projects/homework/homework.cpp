
#include <iostream>

using std::cout;

int main()
{
    const int n = 5;
    int calendar_number = 6;
    int array[n][n];

    for (int i = 0, j; i < n; ++i) {
        for (j = 0; j < n; ++j) {
            array[i][j] = i * j;
        }
    }

    cout << "Array data:\n";
    for (int i = 0, j; i < n; ++i) {
        for (j = 0; j < n; ++j) {
            cout << array[i][j] << ",";
        }
        cout << "\n";
    }

    cout << "row sum (index = calendar num) = ";
    int sum = 0;
    int row_index = 6 % n;
    for (int j = 0; j < n; ++j) {
        sum += array[row_index][j];
    }
    cout << sum;

}

